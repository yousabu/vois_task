# Scrum Boards Deployment

## What I have done so far

- I have developed a basic application using Java for the backend and Angular for the frontend, alongside PostgreSQL for the database.
- I have created a Docker Compose file that runs all the components.
- I have built the deployment files needed to deploy on a Kubernetes cluster or use a Helm chart.
- I have created a pipeline to build and deploy this application on the AWS cloud provider.



### Main criteria
| criterion | completed | artifacts |
|---|---|---|
| Dockerfile that build the JavaSpring and make it lightweight  | yes | [Dockerfile](./scrum_backend/Dockerfile) |
Dockerfile that build the Angular and make it lightweight  | yes | [Dockerfile](./scrum_ui/Dockerfile) |
| Pipeline jobs (.gitlab-ci.yml) to build the app using dockerfile and Push the Image to GitLab Contaner Registry after that it deploy to AWS EC2. | yes | [.gitlab-ci.yml](./.gitlab-ci.yml) |
| Docker compose file that contains both application (Frontend, Backend) and PostgreSQL database | yes | [docker-compose.yml](./docker-compose.yml) |
| Helm manifests for kubernetes to deploy the app using them on kubernetes with adding config to support volume persistence and exposing service to the public  | yes | [backend_chart](./backend_chart), [ui_chart](./ui_chart)|


### Application screenshot

![app screenshot](./images/app1.png)
![app screenshot](./images/app2.png)
![app screenshot](./images/app3.png)

## Deployment instructions

### Prerequisites

PREFACE: This project's helm chart was created using Helm v3 and tested against
Kubernetes v1.28.2.

#### In-cluster prerequisites

- A load balancer, on cloud platforms load balancers are usually provided by
    default but on bare metal clusters you need to install one. I used
    [MetalLB](https://metallb.universe.tf/) for this project.
- [nginx-ingress](https://artifacthub.io/packages/helm/nginx-ingress/haproxy-ingress)
    : a high performance ingress controller to expose the application
    externally using the load balancer.

#### On client prerequisites

- [docker](https://docs.docker.com/get-docker/)
- [docker-compose](https://docs.docker.com/compose/)
- [helm](https://helm.sh/)



### docker-compose.yml usage
- docker composer file that run all the component.(Angular,JavaSpring,PostgreSQL).

#### Initializing the project

The [`docker-compose.yml`](./docker-compose.yml) file populates container
environment variables 


- Run the compose file and wait for the database to initialize

    ```console
    $ docker-compose up -d \
    > && sleep 30 && docker container ls # delay for db creation
    Building server
    [+] Building 0.7s (13/13) FINISHED
    ...
    WARNING: Image for service server was built because it did not already exist. To rebuild this image you must use `docker-compose build` or `docker-compose up --build`.
    Starting scrum-postgres ... done
    Starting scrum-app   ... done
    Starting go-serve_server_1   ... done
    CONTAINER ID   IMAGE                                                         COMMAND                  CREATED       STATUS       PORTS                                       NAMES
    3b8471ad2b82   registry.gitlab.com/yousabu/vois_task/scrum_frontend:latest   "nginx -g 'daemon of…"   30 seconds ago    Up 15 seconds    0.0.0.0:4200->80/tcp, :::4200->80/tcp       scrum-ui
    6b238cd08525   registry.gitlab.com/yousabu/vois_task/scrum_backend:latest    "java -jar app.jar"      30 seconds ago    Up 15 seconds   0.0.0.0:8080->8080/tcp, :::8080->8080/tcp   scrum-app
    acbad96dbe5f   postgres:9.6-alpine                                           "docker-entrypoint.s…"   30 seconds ago    Up 15 seconds    0.0.0.0:5432->5432/tcp, :::5432->5432/tcp   scrum-postgres
    ```



#### Docker compose tl;dr

- Starting the application

    ```shell
    docker-compose start
    ```

- Stopping the application

    ```shell
    docker-compose stop
    ```

- Removing the application

    ```shell
    docker-compose down
    ```

- Removing the application and its volumes

    ```shell
    docker-compose down -v
    ```

- Running a command in the application container

    ```shell
    docker-compose exec server <command>
    ```

### GitLab (CI/CD) pipeline

The GitLab pipeline is configured to run on every push to the `main` branch and:
- If there is a change in [`scrum_ui`](./scrum_ui) OR [`scrum_backend`](./scrum_backend)  .Pipeline will start build Dockerfile which belong to this directory. 
- Build the application image from Dockerfile.
- Tag the image with version number variable and build number.
- Test Backend Image.
- Deploy Both Backend & Frontend Using Docker-Compose.
- Push the images to DockerHub GitLab Container Registry

#### Prerequisite Install GitLab Runner
    ```console
    # Download the binary for your system
    sudo curl -L --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64

    # Give it permission to execute
    sudo chmod +x /usr/local/bin/gitlab-runner

    # Create a GitLab Runner user
    sudo useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash

    # Install and run as a service
    sudo gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner
    sudo gitlab-runner start
    gitlab-runner register
    ```



#### Deploying the application Using Helm

- Use default kubectl namespace 

    ```console
    youssef@devops:~/Documents/vst$ helm install backend backend_chart
    NAME: backend
    LAST DEPLOYED: Sun Oct  1 15:47:22 2023
    NAMESPACE: default
    STATUS: deployed
    REVISION: 1
    TEST SUITE: None
    ```
    ```console
    youssef@devops:~/Documents/vst$ helm install frontend ui_chart
    NAME: frontend
    LAST DEPLOYED: Sun Oct  1 15:55:57 2023
    NAMESPACE: default
    STATUS: deployed
    REVISION: 1
    TEST SUITE: None
    ```

    ```console
    youssef@devops:~/Documents/vst$ kubectl get pod
    NAME                         READY   STATUS             RESTARTS        AGE
    postgres-0                   1/1     Running            0               12m
    scrum-back-5879469d6-c9rqd   1/1     Running            2 (10s ago)     12m
    scrum-ui-7476bb87f5-kcxsf    1/1     Running            1 (20s ago)     10m
    ```


<sup>
The restart is okay because the server doesn't wait for the database and
crashes.
</sup>

### In case the data retrieval is slow, 
    1) what might cause this issue? 
    2) what is the solution you recommend identifying the issue and to solve it as much as you can.
## Solution:
1) Which may cause the problem is:
    - Inefficient or misconfigured database connection pooling, which can lead to slow data retrieval.
    - Slow database queries, indexing problems, or database server overload, which can slow down data retrieval.
    - High network latency, especially if the application and the database server are on different servers or in different geographical regions.
    - Poorly optimized code in the Spring Boot application, causing slow data retrieval.
    - Inefficient Angular code, excessive HTTP requests, or rendering bottlenecks in the frontend, contributing to slow data retrieval.
    - Lack of monitoring and logging, making it difficult to identify the root cause of slow data retrieval.
    - Lack of caching or incorrect caching strategies, resulting in repeated slow data retrieval.

2) what is the solution you recommend identifying the issue and to solve it as much as you can?
    - Optimize database queries and ensure they are properly indexed and Consider caching frequently accessed data using tools like Redis.
    - Optimize the network infrastructure to reduce latency.
    - Optimize code by using appropriate data structures and algorithms.
    - Implement client-side caching to store and reuse data that doesn't change frequently.
    - Implement load balancing to distribute incoming requests across multiple server instances.Use connection pooling and database sharding to handle concurrent database connections efficiently.
    - Implement comprehensive logging and monitoring using tools like Prometheus and Grafana or application-specific tools
    - Implement caching for frequently accessed data using techniques like server-side caching (Redis) or client-side caching
    